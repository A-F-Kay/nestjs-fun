import { DEFAULT_LIMIT } from 'src/const/pagination';
import { PaginationQueryDto } from 'src/dto/query_params';

export const getStrictPagination = ({
  page = 1,
  limit = DEFAULT_LIMIT,
  unlimit = false,
}: PaginationQueryDto): Required<PaginationQueryDto> => {
  return { page, limit, unlimit };
};
