import { Injectable } from '@nestjs/common';
import { PaginationQueryDto } from 'src/dto/query_params';
import { getRandomIntFromRange } from 'src/utils/math';

type Cat = {
  id: number;
  name: string;
  age: number;
};

const makeCats = (length: number): Cat[] => {
  const names = ['Barsik', 'Boris', 'Vasiliy', 'Nyasha'];

  return new Array(length).fill(null).map((_, idx) => {
    const name = names[idx % names.length];

    return { id: idx + 1, name, age: getRandomIntFromRange(1, 15) };
  });
};

@Injectable()
export class CatsService {
  private readonly cats = makeCats(200);

  public list({ page, limit, unlimit }: Required<PaginationQueryDto>): Cat[] {
    if (unlimit) {
      return this.cats;
    }

    const start = (page - 1) * limit;
    const end = page * limit;

    return this.cats.slice(start, end);
  }
}
