import { Controller, Get, Inject, Query } from '@nestjs/common';
import { PaginationQueryDto } from 'src/dto/query_params';
import { getStrictPagination } from 'src/utils/pagination';
import { CatsService } from './cats.service';

@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Get()
  async get(@Query() paginationQuery: PaginationQueryDto) {
    const pagination = getStrictPagination(paginationQuery);

    return this.catsService.list(pagination);
  }
}
